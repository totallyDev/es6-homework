import { showModal } from "./modal";

export function showWinnerModal(fighter) {
	const showWinner = {
		title: 'WINNER',
		bodyElement: fighter.name
	}

	showModal(showWinner);
}
